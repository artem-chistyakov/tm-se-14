package ru.chistyakov.tm.loader;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.endpoint.ProjectEndpoint;
import ru.chistyakov.tm.endpoint.SessionEndpoint;
import ru.chistyakov.tm.endpoint.TaskEndpoint;
import ru.chistyakov.tm.endpoint.UserEndpoint;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.service.*;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.HashMap;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private PropertiesService propertiesService = new PropertiesService();

    @NotNull
    private EntityManagerFactory entityManagerFactory = factory();

    @NotNull
    private final IProjectService projectService = new ProjectService(entityManagerFactory);
    @NotNull
    private final IDomainService domainService = new DomainService(entityManagerFactory);
    @NotNull
    private final ITaskService taskService = new TaskService(entityManagerFactory);
    @NotNull
    private final IUserService userService = new UserService(entityManagerFactory);
    @NotNull
    private final ISessionService sessionService = new SessionService(propertiesService, entityManagerFactory);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(userService, sessionService, domainService);

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    private void init() {
        propertiesService.init();
        Endpoint.publish(projectEndpoint.getURL(), projectEndpoint);
        Endpoint.publish(taskEndpoint.getURL(), taskEndpoint);
        Endpoint.publish(userEndpoint.getURL(), userEndpoint);
        Endpoint.publish(sessionEndpoint.getURL(), sessionEndpoint);
    }

    private EntityManagerFactory factory() {
        propertiesService.init();
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertiesService.getJdbcDriver());
        settings.put(Environment.URL, propertiesService.getJdbcUrl());
        settings.put(Environment.USER, propertiesService.getJdbcUser());
        settings.put(Environment.PASS, propertiesService.getJdbcPassword());
        settings.put(Environment.DIALECT,
                "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    public void start() {
        init();
    }
}

