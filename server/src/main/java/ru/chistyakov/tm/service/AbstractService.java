package ru.chistyakov.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IService;
import ru.chistyakov.tm.dto.AbstractDTO;
import ru.chistyakov.tm.utility.EntityFactoryUtil;

import javax.persistence.EntityManagerFactory;
import java.text.SimpleDateFormat;
import java.util.Collection;


public abstract class AbstractService<T extends AbstractDTO> implements IService<T> {

    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public abstract boolean merge(T t);

    public abstract boolean persist(T t);

    public abstract @Nullable Collection<T> findAll();
}
