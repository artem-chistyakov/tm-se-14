package ru.chistyakov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.repository.*;
import ru.chistyakov.tm.utility.EntityFactoryUtil;

import javax.persistence.EntityManagerFactory;
import java.io.*;
import java.util.Collection;
import java.util.Iterator;

public class DomainService implements ru.chistyakov.tm.api.IDomainService {

    private final EntityManagerFactory entityManagerFactory;

    public DomainService(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public void serializateDomain() {
        try (@NotNull final FileOutputStream projectFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "projectFile.txt");
             @NotNull final FileOutputStream taskFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "taskFile.txt");
             @NotNull final FileOutputStream userFileOutputStream = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userFile.txt");
        ) {
            final IProjectRepository projectRepository = new ProjectRepository(entityManagerFactory.createEntityManager());
            final ITaskRepository taskRepository = new TaskRepository(entityManagerFactory.createEntityManager());
            final IUserRepository userRepository = new UserRepository(entityManagerFactory.createEntityManager());
            final ObjectOutputStream projectObjectOutputStream = new ObjectOutputStream(projectFileOutputStream);
            projectObjectOutputStream.writeObject(projectRepository.findAll());
            final ObjectOutputStream taskObjectOutputStream = new ObjectOutputStream(taskFileOutputStream);
            taskObjectOutputStream.writeObject(taskRepository.findAll());
            final ObjectOutputStream userObjectOutputStream = new ObjectOutputStream(userFileOutputStream);
            userObjectOutputStream.writeObject(userRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deserializateDomain() {
        try (final FileInputStream userFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "projectFile.txt");
             final FileInputStream projectFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "taskFile.txt");
             final FileInputStream taskFileInputStream = new FileInputStream(new File(".").getAbsolutePath() + File.separator + "userFile.txt");
        ) {
            final IProjectRepository projectRepository = new ProjectRepository(entityManagerFactory.createEntityManager());
            final ITaskRepository taskRepository = new TaskRepository(entityManagerFactory.createEntityManager());
            final IUserRepository userRepository = new UserRepository(entityManagerFactory.createEntityManager());
            final ObjectInputStream userObjectInputStream = new ObjectInputStream(userFileInputStream);
            final ObjectInputStream projectObjectInputStream = new ObjectInputStream(projectFileInputStream);
            final ObjectInputStream taskObjectInputStream = new ObjectInputStream(taskFileInputStream);
            final Collection<UserDTO> userDTOCollection = (Collection<UserDTO>) userObjectInputStream.readObject();
            for (final UserDTO userDTO : userDTOCollection) {
                userRepository.persist(userDTO);
            }
            final Collection<ProjectDTO> projectDTOCollection = (Collection<ProjectDTO>) projectObjectInputStream.readObject();
            for (final ProjectDTO projectDTO : projectDTOCollection) {
                projectRepository.persist(projectDTO);
            }
            final Collection<TaskDTO> taskDTOCollection = (Collection<TaskDTO>) taskObjectInputStream.readObject();
            for (TaskDTO taskDTO : taskDTOCollection) {
                taskRepository.persist(taskDTO);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDomainJacksonXml() {
        try (FileOutputStream fOSProject = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
             FileOutputStream fOSTask = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
             FileOutputStream fOSUser = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
        ) {
            final IProjectRepository projectRepository = new ProjectRepository(entityManagerFactory.createEntityManager());
            final ITaskRepository taskRepository = new TaskRepository(entityManagerFactory.createEntityManager());
            final IUserRepository userRepository = new UserRepository(entityManagerFactory.createEntityManager());
            final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            final XmlMapper xmlMapper = new XmlMapper(module);
            final String projectJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectRepository.findAll());
            final String taskJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskRepository.findAll());
            final String userJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userRepository.findAll());
            fOSProject.write(projectJsonArray.getBytes());
            fOSProject.flush();
            fOSTask.write(taskJsonArray.getBytes());
            fOSTask.flush();
            fOSUser.write(userJsonArray.getBytes());
            fOSUser.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadDomainJacksonXml() {
        try {
            final IProjectRepository projectRepository = new ProjectRepository(entityManagerFactory.createEntityManager());
            final ITaskRepository taskRepository = new TaskRepository(entityManagerFactory.createEntityManager());
            final IUserRepository userRepository = new UserRepository(entityManagerFactory.createEntityManager());
            final JacksonXmlModule jacksonXmlModule = new JacksonXmlModule();
            jacksonXmlModule.setDefaultUseWrapper(false);
            final XmlMapper xmlMapper = new XmlMapper(jacksonXmlModule);
            final Iterator<ProjectDTO> projectIterator = xmlMapper.readerFor(ProjectDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
            final Iterator<TaskDTO> taskIterator = xmlMapper.readerFor(TaskDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
            final Iterator<UserDTO> userIterator = xmlMapper.readerFor(UserDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "userXml.xml");
            while (userIterator.hasNext()) {
                userRepository.persist(userIterator.next());
            }
            while (projectIterator.hasNext()) {
                projectRepository.persist(projectIterator.next());
            }
            while (taskIterator.hasNext()) {
                taskRepository.persist(taskIterator.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveDomainJacksonJson() {
        try (FileOutputStream fOSProject = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userJson.json");
             FileOutputStream fOSTask = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userJson.json");
             FileOutputStream fOSUser = new FileOutputStream(new File(".").getAbsolutePath() + File.separator + "userJson.json");
        ) {
            final IProjectRepository projectRepository = new ProjectRepository(entityManagerFactory.createEntityManager());
            final ITaskRepository taskRepository = new TaskRepository(entityManagerFactory.createEntityManager());
            final IUserRepository userRepository = new UserRepository(entityManagerFactory.createEntityManager());
            final ObjectMapper objectMapper = new ObjectMapper();
            final String projectJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projectRepository.findAll());
            final String taskJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(taskRepository.findAll());
            final String userJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userRepository.findAll());
            fOSProject.write(projectJsonArray.getBytes());
            fOSTask.write(taskJsonArray.getBytes());
            fOSUser.write(userJsonArray.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadDomainJacksonJson() {
        try {
            final IProjectRepository projectRepository = new ProjectRepository(entityManagerFactory.createEntityManager());
            final ITaskRepository taskRepository = new TaskRepository(entityManagerFactory.createEntityManager());
            final IUserRepository userRepository = new UserRepository(entityManagerFactory.createEntityManager());
            final ObjectMapper objectMapper = new ObjectMapper();
            final Iterator<ProjectDTO> projectIterator = objectMapper.readerFor(ProjectDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "userJson.json");
            final Iterator<TaskDTO> taskIterator = objectMapper.readerFor(TaskDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "userJson.json");
            final Iterator<UserDTO> userIterator = objectMapper.readerFor(UserDTO.class).readValues(new File(".").getAbsolutePath() + File.separator + "userJson.json");
            while (userIterator.hasNext()) {
                userRepository.persist(userIterator.next());
            }
            while (projectIterator.hasNext()) {
                projectRepository.persist(projectIterator.next());
            }
            while (taskIterator.hasNext()) {
                taskRepository.persist(taskIterator.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}