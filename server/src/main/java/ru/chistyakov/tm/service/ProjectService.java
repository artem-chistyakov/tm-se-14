package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.repository.IProjectRepository;
import ru.chistyakov.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collection;

public final class ProjectService extends AbstractService<ProjectDTO> implements IProjectService {

    private final EntityManagerFactory entityManagerFactory;

    public ProjectService(@NotNull final  EntityManagerFactory entityManagerFactory){
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public boolean merge(@Nullable final ProjectDTO project) {
        if (project == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.merge(project);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean persist(@Nullable final ProjectDTO project) {
        if (project == null) return false;
        if(project.getName()==null || project.getName().isEmpty()) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.persist(project);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean removeAll(@Nullable final String userId) {
        if (userId == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.removeAll(userId);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        final Project removedProject = projectRepository.findOne(id);
        projectRepository.remove(removedProject);
        entityManager.getTransaction().commit();
        return true;
    }


    @Override
    public @Nullable ProjectDTO findOne(@Nullable final String id) {
        if (id == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.findOneDto(id);
    }

    @Override
    public boolean update(@Nullable final ProjectDTO project) {
        if (project == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.update(project);
        entityManager.getTransaction().commit();
        return true;
    }


    @Override
    public @Nullable Collection<ProjectDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public @Nullable Collection<ProjectDTO> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.findAllInOrder(userId, "date_begin");
    }

    @Override
    public @Nullable Collection<ProjectDTO> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.findAllInOrder(userId, "date_end");
    }

    @Override
    public @Nullable Collection<ProjectDTO> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.findAllInOrder(userId, "readiness_status");
    }

    @Override
    public @Nullable Collection<ProjectDTO> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || part == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.findByPartNameOrDescription(userId, part);
    }

    public @Nullable Collection<ProjectDTO> findAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        return projectRepository.findAll();
    }
}