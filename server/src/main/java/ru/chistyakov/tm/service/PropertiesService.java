package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesService {

    @NotNull
    final private String propertiesFile = "/application.properties";

    @NotNull
    final private Properties properties = new Properties();

    public void init() {
        try {
            final InputStream inputStream = PropertiesService.class.getResourceAsStream(propertiesFile);
            properties.load(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public String getSALT() {
        return properties.getProperty("session.salt");
    }

    @Nullable
    public Integer getCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

    @NotNull
    public String getJdbcUrl(){return properties.getProperty("jdbc.url");}
    @NotNull
    public String getJdbcUser(){return properties.getProperty("jdbc.user");}
    @NotNull
    public String getJdbcPassword(){return properties.getProperty("jdbc.password");}
    @NotNull
    public String getJdbcDriver(){return properties.getProperty("jdbc.driver");}
}

