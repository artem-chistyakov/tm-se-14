package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.repository.ITaskRepository;
import ru.chistyakov.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collection;


public final class TaskService extends AbstractService<TaskDTO> implements ITaskService {

    private final EntityManagerFactory entityManagerFactory;

    public TaskService(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public boolean merge(final TaskDTO taskDTO) {
        if (taskDTO == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.merge(taskDTO);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean persist(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.persist(taskDTO);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public @Nullable TaskDTO findOne(@Nullable final String id) {
        if (id == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        return taskRepository.findOneDto(id);
    }

    @Override
    public @Nullable Collection<TaskDTO> findAllByUserId(@Nullable final String userId) {
        if (userId == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public boolean update(@Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.update(taskDTO);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean remove(@Nullable final String id) {
        if (id == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        final Task task = taskRepository.findOne(id);
        taskRepository.remove(task);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean removeAll(@Nullable final String userId) {
        if (userId == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.removeAll(userId);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public @Nullable Collection<TaskDTO> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        return taskRepository.findAllInOrder(userId, "date_begin");
    }

    @Override
    public @Nullable Collection<TaskDTO> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        return taskRepository.findAllInOrder(userId, "date_end");
    }

    @Override
    public @Nullable Collection<TaskDTO> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        return taskRepository.findAllInOrder(userId, "readiness_status");
    }

    @Override
    public @Nullable Collection<TaskDTO> findByPartNameOrDescription(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || part == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        return taskRepository.findByPartNameOrDescription(userId, part);
    }

    public @Nullable Collection<TaskDTO> findAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        final ITaskRepository taskRepository = new TaskRepository(entityManager);
        return taskRepository.findAll();
    }
}
