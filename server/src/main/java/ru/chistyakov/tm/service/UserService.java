package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.repository.IUserRepository;
import ru.chistyakov.tm.repository.UserRepository;
import ru.chistyakov.tm.utility.PasswordParser;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collection;

public final class UserService extends AbstractService<UserDTO> implements IUserService {

    private final EntityManagerFactory entityManagerFactory;

    public UserService(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public boolean merge(final UserDTO userDTO) {
        if (userDTO == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.merge(userDTO);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public boolean persist(final UserDTO userDTO) {
        if (userDTO == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.persist(userDTO);
        entityManager.getTransaction().commit();
        return true;
    }

    @Override
    public @Nullable User authoriseUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return null;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        final User user = userRepository.findOneWithLoginPassword(login, passwordMD5);
        return user;
    }

    @Override
    public boolean updateUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.update(userDTO);
        entityManager.getTransaction().commit();
        return true;
    }


    @Override
    public @Nullable UserDTO findWithLoginPassword(@Nullable String login, @Nullable String password) {
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        return mapUserToUserDto(userRepository.findOneWithLoginPassword(login, passwordMD5));
    }

    @Override
    public boolean registryUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        if (!userRepository.findOneWithLogin(userDTO.getLogin()).isEmpty()) return false;
        userRepository.persist(mapUserDtoToUser(userDTO));
        entityManager.getTransaction().commit();
        return true;
    }


    @Override
    public @Nullable UserDTO findById(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        return mapUserToUserDto(userRepository.findOne(userId));
    }

    @Override
    public void deleteAccount(@NotNull String userID) {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.removeById(userID);
        entityManager.getTransaction().commit();
    }

    @Override
    public @Nullable Collection<UserDTO> findAll() {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final IUserRepository userRepository = new UserRepository(entityManager);
        return userRepository.findAll();
    }

    private @NotNull User mapUserDtoToUser(UserDTO userDTO) {
        final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        user.setRoleType(userDTO.getRoleType());
        return user;
    }

    private @NotNull UserDTO mapUserToUserDto(User user) {
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setRoleType(user.getRoleType());
        return userDTO;
    }
}
