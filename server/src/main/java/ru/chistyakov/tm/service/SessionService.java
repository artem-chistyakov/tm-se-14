package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.repository.SessionRepository;
import ru.chistyakov.tm.repository.UserRepository;
import ru.chistyakov.tm.utility.SignatureUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public final class SessionService implements ISessionService {
    @NotNull
    private final EntityManagerFactory entityManagerFactory;
    @NotNull
    private final PropertiesService propertiesService;

    public SessionService(@NotNull final PropertiesService propertiesService, @NotNull final EntityManagerFactory entityManagerFactory) {
        this.propertiesService = propertiesService;
        this.entityManagerFactory = entityManagerFactory;
    }


    @Override
    public void validate(@Nullable final SessionDTO session) throws IllegalAccessException {
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final SessionRepository sessionRepository = new SessionRepository(entityManager);
        if (session == null) throw new IllegalAccessException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new IllegalAccessException();
        if (session.getTimestamp() == null) throw new IllegalAccessException();
        if (session.getSignature() == null || session.getSignature().isEmpty())
            throw new IllegalAccessException();
        final String signatureSource = session.getSignature();
        final SessionDTO sessionClone = (SessionDTO) session.clone();
        sessionClone.setSignature(null);
        if (propertiesService.getSALT() == null || propertiesService.getCycle() == null)
            throw new IllegalArgumentException("Ошибка загрузки проперти файла");
        final String signatureTarget = SignatureUtil.sign(sessionClone, propertiesService.getSALT(), propertiesService.getCycle());
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new IllegalAccessException();
        if (sessionRepository.contains(session.getId()) == null) throw new IllegalAccessException();

    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public SessionDTO open(@Nullable final String userId, @Nullable final RoleType roleType) {
        if (userId == null || roleType == null) return null;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final SessionRepository sessionRepository = new SessionRepository(entityManager);
        final SessionDTO session = new SessionDTO();
        session.setUserId(userId);
        session.setRoleType(roleType);
        session.setSignature(SignatureUtil.sign(session, propertiesService.getSALT(), propertiesService.getCycle()));
        entityManager.getTransaction().begin();
        sessionRepository.create(mapSessionDtoToSession(session));
        entityManager.getTransaction().commit();
        return session;
    }

    @Override
    public boolean close(Session session) {
        if (session == null) return false;
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final SessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        sessionRepository.delete(session);
        entityManager.getTransaction().commit();
        return true;
    }

    private Session mapSessionDtoToSession(@NotNull final SessionDTO sessionDTO) {
        final Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        session.setRoleType(sessionDTO.getRoleType());
        final EntityManager entityManager = entityManagerFactory.createEntityManager();
        final UserRepository userRepository = new UserRepository(entityManager);
        session.setUser(userRepository.findOne(sessionDTO.getUserId()));
        return session;
    }
}
