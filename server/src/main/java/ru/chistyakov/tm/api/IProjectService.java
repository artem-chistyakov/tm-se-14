package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectService extends IService<ProjectDTO> {

    @WebMethod
    boolean merge(ProjectDTO projectDTO);

    @WebMethod
    boolean removeAll(@Nullable String userId);

    @WebMethod
    boolean persist(ProjectDTO projectDTO);

    @WebMethod
    boolean remove(@Nullable String id);

    @WebMethod
    @Nullable
    ProjectDTO findOne(@Nullable String id);

    @WebMethod
    boolean update(final ProjectDTO project);

    @WebMethod
    @Nullable
    Collection<ProjectDTO> findAllByUserId(String userId);

    @WebMethod
    @Nullable
    Collection<ProjectDTO> findAllInOrderDateBegin(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<ProjectDTO> findAllInOrderDateEnd(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<ProjectDTO> findAllInOrderReadinessStatus(@Nullable String userId);

    @WebMethod
    @Nullable
    Collection<ProjectDTO> findByPartNameOrDescription(@Nullable String userId, String part);

}