package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;


public interface ITaskService extends IService<TaskDTO> {

    boolean persist(TaskDTO task);


    boolean merge(TaskDTO task);

    @Nullable
    TaskDTO findOne(@NotNull String id);


    @Nullable
    Collection<TaskDTO> findAllByUserId(@Nullable String userId);


    boolean update(@Nullable TaskDTO taskDTO);


    boolean remove(@Nullable String id);


    @Nullable
    Collection<TaskDTO> findAllInOrderDateBegin(@NotNull String userId);


    @Nullable
    Collection<TaskDTO> findAllInOrderDateEnd(@NotNull String userId);


    @Nullable
    Collection<TaskDTO> findAllInOrderReadinessStatus(@NotNull String userId);


    @Nullable
    Collection<TaskDTO> findByPartNameOrDescription(@NotNull String userId, @NotNull String part);

    boolean removeAll(@NotNull String userId);
}
