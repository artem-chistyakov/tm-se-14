package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;

public interface IUserService extends IService<UserDTO> {

    @Nullable
    User authoriseUser(@Nullable String name, @Nullable String password);

    boolean updateUser(@Nullable UserDTO userDTO);

    @Nullable UserDTO findWithLoginPassword(@Nullable String login, @Nullable String password);

    boolean registryUser(@Nullable UserDTO userDTO);

    @Nullable UserDTO findById(@Nullable final String userId);

    void deleteAccount(@NotNull final String userID);
}
