package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.entity.Session;
import ru.chistyakov.tm.enumerate.RoleType;

public interface ISessionService {


    void validate(@Nullable SessionDTO session) throws IllegalAccessException;

    boolean isValid(@Nullable SessionDTO session);

    SessionDTO open(@Nullable String userId, RoleType roleType);

    boolean close(Session session);
}
