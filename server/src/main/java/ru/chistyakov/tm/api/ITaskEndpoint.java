package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {
    @NotNull
    String getURL();

    @WebMethod
    boolean persistTask(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session,
                        @Nullable @WebParam(name = "task", partName = "task") TaskDTO task);

    @WebMethod
    @Nullable boolean mergeTask(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session,
                                @Nullable @WebParam(name = "task", partName = "task") TaskDTO task);

    @WebMethod
    @Nullable boolean removeTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                 @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId);

    @WebMethod
    @Nullable
    TaskDTO findOneTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                        @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId);

    @WebMethod
    @Nullable Collection<TaskDTO> findAllTaskByUserId(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session);

    @WebMethod
    @Nullable boolean updateTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                 @Nullable @WebParam(name = "taskDTO", partName = "taskDTO") final TaskDTO taskDTO);

    @WebMethod
    void removeAllTask(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session);

    @WebMethod
    @Nullable Collection<TaskDTO> findAllTaskInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session);

    @WebMethod
    @Nullable Collection<TaskDTO> findAllTaskInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session);

    @WebMethod
    @Nullable Collection<TaskDTO> findAllTaskInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session);

    @WebMethod
    @Nullable Collection<TaskDTO> findTaskByPartNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session,
                                                                  @Nullable @WebParam(name = "part", partName = "part") String part);

    @WebMethod
    @Nullable Collection<TaskDTO> findAllTask(@Nullable @WebParam(name = "session", partName = "session") SessionDTO session);
}
