package ru.chistyakov.tm.api;

import ru.chistyakov.tm.dto.AbstractDTO;

public interface IRepository<T extends AbstractDTO> {

    void persist(T t);

    void merge(T t);
}
