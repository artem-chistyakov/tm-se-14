package ru.chistyakov.tm.repository;

import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.dto.AbstractDTO;


public abstract class AbstractRepository<T extends AbstractDTO> implements IRepository<T> {

    public abstract void persist(T t);

    public abstract void merge(T t);

}
