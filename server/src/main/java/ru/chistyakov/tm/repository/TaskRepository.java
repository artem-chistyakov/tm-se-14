package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    final private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public void persist(@NotNull final TaskDTO taskDTO) {
        entityManager.persist(taskDTO);
    }

    @Override
    public @Nullable Task findOne(@NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    @Nullable
    public TaskDTO findOneDto(@NotNull String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Override
    public boolean update(@NotNull final TaskDTO taskDTO) {
        return entityManager.createQuery("update TaskDTO t set t.name = :name,t.description = :desc,t.dateBeginTask=:dateBegin,t.dateEndTask=:dateEnd,readinessStatus=:readinessStatus").
                setParameter("name", taskDTO.getName()).setParameter("desc", taskDTO.getDescription()).setParameter("dateBegin", taskDTO.getDateBeginTask()).
                setParameter("dateEnd", taskDTO.getDateEndTask()).setParameter("readinessStatus", taskDTO.getReadinessStatus()).executeUpdate() > 0;
    }

    @Override
    public @NotNull Collection<TaskDTO> findAllInOrder(@NotNull final String userId, @NotNull final String column) {
        return entityManager.createQuery("Select t from TaskDTO t where t.user_id = :ids order by :order").
                setParameter("ids", userId).setParameter("order", column).getResultList();
    }

    @Override
    public void remove(@NotNull final Task task) {
        entityManager.remove(task);
    }

    @Override
    public boolean removeByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("delete from TaskDTO t where t.userId = :userId and projectId =projectId ").
                setParameter("userId", userId).setParameter("projectId", projectId).executeUpdate() > 0;
    }

    @Override
    public void removeAll(@NotNull String userId) {
        List<Task> taskList = entityManager.createQuery("select t from Task t where t.user.id =:userId").setParameter("userId", userId).getResultList();
        for (Task task : taskList) {
            entityManager.remove(task);
        }
    }

    @Override
    public @Nullable Collection<TaskDTO> findAllByUserId(@NotNull String userId) {
        return entityManager.createQuery("select t from TaskDTO t where t.userId = :userId").setParameter("userId", userId).getResultList();
    }

    @Override
    public @Nullable Collection<TaskDTO> findByPartNameOrDescription(@NotNull String userId, @Nullable String part) {
        return entityManager.createQuery("select t from TaskDTO t where t.userId = :userId and" +
                "t.name like:part or t.description like :part").setParameter("userId", userId).setParameter("part", part).getResultList();
    }

    @Override
    public Collection<TaskDTO> findAll() {
        return entityManager.createQuery("select t from TaskDTO t").getResultList();
    }

    @Override
    public void merge(TaskDTO taskDTO) {
        entityManager.merge(taskDTO);
    }
}
