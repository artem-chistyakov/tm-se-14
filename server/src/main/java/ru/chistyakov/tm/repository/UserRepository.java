package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class UserRepository implements IUserRepository {

    @Override
    public @Nullable User findOne(@NotNull String id) {
        return entityManager.find(User.class,id);
    }

    final private EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void remove(@NotNull final UserDTO userDTO) {
        entityManager.remove(userDTO);
    }

    @Override
    public void removeById(@NotNull String userId) {
        final User user = entityManager.find(User.class,userId);
       entityManager.remove(user);
    }

    @Override
    public void persist(@NotNull User user) {
        entityManager.persist(user);
    }

    @Override
    public boolean update(@NotNull UserDTO userDTO) {
        return entityManager.createQuery("update UserDTO u set u.login = :login,u.password=:passwordHash,roleType=:roleType").
                setParameter("login", userDTO.getLogin()).setParameter("passwordHash", userDTO.getPassword()).setParameter("roleType", userDTO.getRoleType()).executeUpdate() > 0;
    }

    @Override
    public User findOneWithLoginPassword(@Nullable String login, @Nullable String password) {
        return (User) entityManager.createQuery("select u from User u where u.login = :login and u.password=:passwordHash").
                setParameter("login", login).setParameter("passwordHash", password).getSingleResult();
    }

    @Override
    public @NotNull Collection<UserDTO> findAll() {
        return entityManager.createQuery("select u from UserDTO u ").getResultList();
    }

    @Override
    public void persist(UserDTO userDTO) {

    }
    @Override
    public List<User> findOneWithLogin(@Nullable String login) {
        return  entityManager.createQuery("select u from User u where u.login = :login ").
                setParameter("login", login).getResultList();
    }
    @Override
    public void merge(UserDTO userDTO) {
        entityManager.merge(userDTO);
    }
}
