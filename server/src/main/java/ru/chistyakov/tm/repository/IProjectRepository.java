package ru.chistyakov.tm.repository;

import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;

import java.util.Collection;

public interface IProjectRepository extends IRepository<ProjectDTO> {

    @Nullable
    Project findOne(@NotNull   String id);

    @Nullable
    ProjectDTO findOneDto(@NotNull   String id);

    @NotNull
    Collection<ProjectDTO> findAllByUserId(@Param("userId") @NotNull String userId);

    @NotNull
    Collection<ProjectDTO> findAllInOrder(@Param("userId") @NotNull String userId, @Param("str") String str);

    boolean update(@NotNull ProjectDTO project);

    void remove(@NotNull Project project);

    void removeAll(@Param("userId") @NotNull String userId);

    @NotNull
    Collection<ProjectDTO> findByPartNameOrDescription(@Param("userId") String userId, @Param("part") String part);

    @NotNull
    Collection<ProjectDTO> findAll();
}
