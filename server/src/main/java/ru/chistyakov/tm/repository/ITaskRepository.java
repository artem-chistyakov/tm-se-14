package ru.chistyakov.tm.repository;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.dto.TaskDTO;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.ReadinessStatus;

import java.util.Collection;
import java.util.Date;

public interface ITaskRepository extends IRepository<TaskDTO> {

    @Nullable
    Task findOne(@NotNull String id);

    @Nullable
    TaskDTO findOneDto(@NotNull String id);

    boolean update(@NotNull TaskDTO taskDTO);

    @NotNull
    Collection<TaskDTO> findAllInOrder(@NotNull String userId, @NotNull String column);

    void remove(@NotNull Task task);

    boolean removeByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAll(@Param("userId") @NotNull String userId);

    @Nullable
    Collection<TaskDTO> findAllByUserId(@NotNull String userId);

    @Nullable
    Collection<TaskDTO> findByPartNameOrDescription(@NotNull String userId, @Nullable String part);

    Collection<TaskDTO> findAll();
}
