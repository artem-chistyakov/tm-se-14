package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    final private EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(@NotNull ProjectDTO project) {
        entityManager.persist(project);
    }

    @Override
    public @Nullable Project findOne(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public @Nullable ProjectDTO findOneDto(@NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public @NotNull Collection<ProjectDTO> findAllByUserId(@NotNull String userId) {
        return entityManager.createQuery("Select p from ProjectDTO p where p.userId = :ids").setParameter("ids", userId).getResultList();
    }

    @Override
    public @NotNull Collection<ProjectDTO> findAllInOrder(@NotNull String userId, String str) {
        return entityManager.createQuery("Select p from ProjectDTO p where p.userId = :ids order by :order").setParameter("ids", userId).setParameter("order", str).getResultList();
    }

    @Override
    public boolean update(@NotNull ProjectDTO project) {
        return entityManager.createQuery("update ProjectDTO p set p.name = :name,p.description = :desc,p.dateBegin=:dateBeginProject,dateEnd=dateEndProject,readinessStatus = :readinessStatus").
                setParameter("name", project.getName()).setParameter("description", project.getDescription()).setParameter("dateBegin", project.getDateBeginProject()).
                setParameter("dateEnd", project.getDateEndProject()).setParameter("readinessStatus", project.getReadinessStatus()).executeUpdate() > 0;
    }

    @Override
    public void remove(@NotNull Project project) {
        entityManager.remove(project);
    }

    @Override
    public void removeAll(@NotNull String userId) {
        List<Project> ptojects = entityManager.createQuery("select p from Project p where p.user.id =:userId").setParameter("userId", userId).getResultList();
        for (Project project : ptojects) {
            entityManager.remove(project);
        }
    }

    @Override
    public @NotNull Collection<ProjectDTO> findByPartNameOrDescription(String userId, String part) {
        return entityManager.createQuery("select p from ProjectDTO p where p.user_id = :ids and p.name like :part or p.description like :part").
                setParameter("ids", userId).setParameter("part", part).getResultList();
    }

    @Override
    public @NotNull Collection<ProjectDTO> findAll() {
        return entityManager.createQuery("select p from ProjectDTO p").getResultList();
    }

    @Override
    public void merge(ProjectDTO project) {
        entityManager.merge(project);
    }
}
