package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;

import javax.persistence.EntityManager;

public class SessionRepository implements ISessionRepository {
    final private EntityManager entityManager;
    public  SessionRepository(@NotNull final  EntityManager entityManager){
        this.entityManager = entityManager;
    }
    @Override
    public void create(@Nullable Session session) {
        entityManager.persist(session);
    }

    @Override
    public Session contains(@Nullable String sessionId) {
        return entityManager.find(Session.class,sessionId);
    }

    @Override
    public void delete(@Nullable Session session) {
        entityManager.remove(session);
    }
}
