package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Session;

public interface ISessionRepository {

    void create(@Nullable Session session);

    Session contains(@Nullable String sessionId);

    void delete(@Nullable Session session);
}
