package ru.chistyakov.tm.repository;

import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Nullable
    User findOne(@NotNull String id);

    void remove(@NotNull UserDTO userDTO);

    void removeById(@NotNull String userId);

    void persist(@NotNull User user);

    boolean update(@NotNull UserDTO userDTO);

    User findOneWithLoginPassword(@Nullable String login, @Param("password") @Nullable String password);

    List<User> findOneWithLogin(@Nullable String login);

    @NotNull
    Collection<UserDTO> findAll();
}
