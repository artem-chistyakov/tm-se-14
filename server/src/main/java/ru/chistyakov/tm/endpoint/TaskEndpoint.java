package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.api.ITaskService;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.ITaskEndpoint")
@XmlRootElement
public final class TaskEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.ITaskEndpoint {
    @NotNull
    private final ISessionService sessionService;
    @NotNull
    private final ITaskService taskService;

    public TaskEndpoint(@NotNull final ITaskService taskService, @NotNull final ISessionService sessionService) {
        this.sessionService = sessionService;
        this.taskService = taskService;
        this.URL = "http://localhost:8080/TaskEndpoint?wsdl";
    }

    @Override
    @WebMethod
    public boolean persistTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                               @Nullable @WebParam(name = "task", partName = "task") final TaskDTO task) {
        if (!sessionService.isValid(session)) return false;
        else return taskService.persist(task);
    }

    @Override
    @WebMethod
    public boolean mergeTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                             @Nullable @WebParam(name = "task", partName = "task") final TaskDTO task) {
        if (!sessionService.isValid(session)) return false;
        else return taskService.merge(task);
    }

    @Override
    @WebMethod
    public boolean removeTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                              @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId) {
        if (session == null) return false;
        if (!sessionService.isValid(session)) return false;
        if (taskId == null) return false;
        return taskService.remove(taskId);
    }

    @Override
    @WebMethod
    public @Nullable TaskDTO findOneTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                         @Nullable @WebParam(name = "taskDTO", partName = "taskDTO") final String taskId) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (taskId == null) return null;
        return taskService.findOne(taskId);
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTaskByUserId(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return taskService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public boolean updateTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                              @Nullable @WebParam(name = "taskDTO", partName = "taskDTO") final TaskDTO taskDTO) {
        if (session == null) return false;
        if (!sessionService.isValid(session)) return false;
        return taskService.update(taskDTO);
    }

    @Override
    @WebMethod
    public void removeAllTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return;
        if (!sessionService.isValid(session)) return;
        if (session.getUserId() == null) return;
        taskService.removeAll(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTaskInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null) return null;
        return taskService.findAllInOrderDateBegin(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTaskInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null) return null;
        return taskService.findAllInOrderDateEnd(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTaskInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null || session.getUserId() == null) return null;
        if (!sessionService.isValid(session)) return null;
        return taskService.findAllInOrderReadinessStatus(session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable Collection<TaskDTO> findTaskByPartNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                                                         @Nullable @WebParam(name = "part", partName = "part") final String part) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        if (session.getUserId() == null || part == null || part.isEmpty()) return null;
        return taskService.findByPartNameOrDescription(session.getUserId(), part);
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<TaskDTO> findAllTask(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return null;
        return taskService.findAll();
    }
}
