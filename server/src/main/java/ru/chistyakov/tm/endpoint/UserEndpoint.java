package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IDomainService;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.dto.UserDTO;
import ru.chistyakov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.IUserEndpoint")
@XmlRootElement
public final class UserEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.IUserEndpoint {

    @NotNull
    private final IDomainService domainService;
    @NotNull
    private final ISessionService sessionService;
    @NotNull
    private final IUserService userService;

    public UserEndpoint(@NotNull final IUserService userService, @NotNull final ISessionService sessionService, @NotNull final IDomainService domainService) {
        this.sessionService = sessionService;
        this.userService = userService;
        this.domainService = domainService;
        this.URL = "http://localhost:8080/UserEndpoint?wsdl";
    }

    @Override
    @WebMethod
    public boolean registryUser(@Nullable @WebParam(name = "userDTO", partName = "userDTO") final UserDTO userDTO) {
        if (userDTO == null) return false;
        return userService.registryUser(userDTO);
    }

    @Override
    @WebMethod
    public SessionDTO authorizeUser(@Nullable @WebParam(name = "login", partName = "login") final String login,
                                    @Nullable @WebParam(name = "password", partName = "password") final String password) {
        if (login == null || password == null) return null;
        final User user = userService.authoriseUser(login, password);
        if (user == null) return null;
        else {
            return sessionService.open(user.getId(), user.getRoleType());
        }
    }

    @Override
    @WebMethod
    public boolean updateUser(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                              @NotNull @WebParam(name = "userDTO", partName = "userDTO") final UserDTO userDTO) {
        if (session == null) return false;
        if (!sessionService.isValid(session)) return false;
        else return userService.updateUser(userDTO);
    }

    @Override
    @WebMethod
    public boolean persistUser(@Nullable @WebParam(name = "user", partName = "user") final UserDTO user) {
        return userService.persist(user);
    }


    @Override
    @WebMethod
    @Nullable
    public UserDTO findUserById(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return userService.findById(session.getUserId());
    }

    @Override
    @WebMethod
    public void deleteAccount(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                              @Nullable @WebParam(name = "userId", partName = "userId") final String userId) {
        if (userId == null) return;
        if (!sessionService.isValid(session)) return;
        userService.deleteAccount(userId);
    }

    @Override
    @WebMethod
    @Nullable
    public UserDTO findWithLoginPassword(@Nullable SessionDTO session, @Nullable String login, @Nullable String password) {
        if (!sessionService.isValid(session)) return null;
        return userService.findWithLoginPassword(login, password);
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<UserDTO> findAllUser(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return null;
        return userService.findAll();
    }

    @Override
    @WebMethod
    public void serializateDomain(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return;
        domainService.serializateDomain();
    }

    @Override
    @WebMethod
    public void deserializateDomain(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return;
        domainService.deserializateDomain();
    }

    @Override
    @WebMethod
    public void saveDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return;
        domainService.saveDomainJacksonXml();
    }

    @Override
    @WebMethod
    public void loadDomainJacksonXml(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return;
        domainService.loadDomainJacksonXml();
    }

    @Override
    @WebMethod
    public void saveDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return;
        domainService.saveDomainJacksonJson();
    }

    @Override
    @WebMethod
    public void loadDomainJacksonJson(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return;
        domainService.loadDomainJacksonJson();
    }
}
