package ru.chistyakov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ISessionService;
import ru.chistyakov.tm.dto.ProjectDTO;
import ru.chistyakov.tm.dto.SessionDTO;
import ru.chistyakov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@WebService(endpointInterface = "ru.chistyakov.tm.api.IProjectEndpoint")
@XmlRootElement
public final class ProjectEndpoint extends AbstractEndpoint implements ru.chistyakov.tm.api.IProjectEndpoint {

    @NotNull
    private final ISessionService sessionService;
    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(@NotNull final IProjectService projectService, @NotNull final ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
        this.URL = "http://localhost:8080/ProjectEndpoint?wsdl";
    }

    @Override
    @WebMethod
    public boolean persistProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                  @Nullable final @WebParam(name = "project", partName = "project") ProjectDTO project) {
        if (!sessionService.isValid(session)) return false;
        else return projectService.persist(project);
    }

    @Override
    @WebMethod
    public boolean mergeProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                @Nullable final @WebParam(name = "project", partName = "project") ProjectDTO project) {
        if (!sessionService.isValid(session)) return false;
        else return projectService.merge(project);
    }

    @Override
    @WebMethod
    public @Nullable ProjectDTO findOneProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                               @Nullable final @WebParam(name = "projectId", partName = "projectId") String projectId) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        else return projectService.findOne(projectId);
    }

    @Override
    @WebMethod
    public boolean removeProject(@Nullable final @WebParam(name = "session", partName = "session") SessionDTO session,
                                 @Nullable final @WebParam(name = "projectId", partName = "projectId") String projectId) {
        if (session == null) return false;
        if (!sessionService.isValid(session)) return false;
        else return projectService.remove(projectId);
    }

    @Override
    @WebMethod
    public boolean updateProject(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                 @Nullable @WebParam(name = "project", partName = "project") final ProjectDTO project) {
        if (!sessionService.isValid(session)) return false;
        else
            return projectService.update(project);
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProjectByUserId(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProject(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (!sessionService.isValid(session)) return null;
        return projectService.findAll();
    }


    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProjectInOrderDateBegin(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findAllInOrderDateBegin(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProjectInOrderDateEnd(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findAllInOrderDateEnd(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findAllProjectInOrderReadinessStatus(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findAllInOrderReadinessStatus(session.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public Collection<ProjectDTO> findByPartProjectNameOrDescription(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
                                                                     @Nullable @WebParam(name = "part", partName = "part") final String part) {
        if (session == null) return null;
        if (!sessionService.isValid(session)) return null;
        return projectService.findByPartNameOrDescription(session.getUserId(), part);
    }

    @Override
    @WebMethod
    public void removeAllProject(@Nullable @WebParam(name = "session", partName = "session") final SessionDTO session) {
        if (session == null) return;
        if (!sessionService.isValid(session)) return;
        projectService.removeAll(session.getUserId());
    }
}