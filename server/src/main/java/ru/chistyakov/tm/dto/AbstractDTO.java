package ru.chistyakov.tm.dto;


import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractDTO implements Serializable {

    @NotNull
    @Id
    @Column(unique = true, name = "id")
    private String id;

    AbstractDTO() {
        id = UUID.randomUUID().toString();
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
