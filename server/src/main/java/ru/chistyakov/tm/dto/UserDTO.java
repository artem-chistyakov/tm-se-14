package ru.chistyakov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "user")
@Entity
@Table(name = "users")
public final class UserDTO extends AbstractDTO implements Serializable {

    @NotNull
    @Column(name = "login", nullable = false)
    private String login = "";
    @NotNull
    @Column(name = "password", nullable = false)
    private String password = "";
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    private RoleType roleType = RoleType.USUAL_USER;
}
