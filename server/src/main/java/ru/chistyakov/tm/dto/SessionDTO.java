package ru.chistyakov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "sessions")
public class SessionDTO extends AbstractDTO implements Cloneable {

    @Nullable
    @Column(name = "user_id", updatable = false, nullable = false)
    private String userId;

    @NotNull
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    private String signature;

    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = "role", updatable = false, nullable = false)
    private RoleType roleType;

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

