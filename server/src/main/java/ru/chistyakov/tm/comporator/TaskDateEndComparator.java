package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.TaskDTO;

import java.util.Comparator;

public final class TaskDateEndComparator implements Comparator<TaskDTO> {
    @Override
    public int compare(@Nullable final TaskDTO o1, @Nullable final TaskDTO o2) {
        if (o1 == null || o2 == null) throw new NullPointerException();
        if (o1.getDateEndTask() == null) return -1;
        if (o2.getDateEndTask() == null) return 1;
        int resultComparing = o1.getDateEndTask().compareTo(o2.getDateEndTask());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        else return resultComparing;
    }
}
