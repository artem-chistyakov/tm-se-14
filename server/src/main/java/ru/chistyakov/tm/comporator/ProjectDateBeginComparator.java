package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.ProjectDTO;

import java.util.Comparator;

public final class ProjectDateBeginComparator implements Comparator<ProjectDTO> {
    @Override
    public int compare(@Nullable final ProjectDTO o1, @Nullable final ProjectDTO o2) {
        if (o1 == null || o2 == null) throw new NullPointerException();
        if (o1.getDateBeginProject() == null) return -1000;
        if (o2.getDateBeginProject() == null) return 1000;
        int resultComparing = o1.getDateBeginProject().compareTo(o2.getDateBeginProject());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        return resultComparing;
    }
}
