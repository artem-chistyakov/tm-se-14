package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.TaskDTO;

import java.util.Comparator;

public final class TaskDateBeginComparator implements Comparator<TaskDTO> {
    @Override
    public int compare(@Nullable final TaskDTO o1, @Nullable final TaskDTO o2) {
        if (o1 == null || o2 == null) throw new NullPointerException();
        if (o1.getDateBeginTask() == null) return -1;
        if (o2.getDateBeginTask() == null) return 1;
        int resultComparing = o1.getDateBeginTask().compareTo(o2.getDateBeginTask());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        return resultComparing;
    }
}
