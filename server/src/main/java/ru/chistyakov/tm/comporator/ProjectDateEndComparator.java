package ru.chistyakov.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.ProjectDTO;

import java.util.Comparator;

public final class ProjectDateEndComparator implements Comparator<ProjectDTO> {
    @Override
    public int compare(@Nullable final ProjectDTO o1, @Nullable final ProjectDTO o2) {
        if (o1 == null || o2 == null) throw new NullPointerException();
        if (o1.getDateEndProject() == null) return -1;
        if (o2.getDateEndProject() == null) return 1;
        int resultComparing = o1.getDateEndProject().compareTo(o2.getDateEndProject());
        if (resultComparing == 0) return o1.getId().compareTo(o2.getId());
        return resultComparing;
    }
}
