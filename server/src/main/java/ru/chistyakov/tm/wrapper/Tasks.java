package ru.chistyakov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.TaskDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "taskDTOS")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Tasks {


    @XmlElement(name = "task")
    private List<TaskDTO> taskDTOS = null;

    @Nullable
    public List<TaskDTO> getTaskDTOS() {
        return taskDTOS;
    }

    public void setTaskDTOS(List<TaskDTO> taskDTOS) {
        this.taskDTOS = taskDTOS;
    }
}
