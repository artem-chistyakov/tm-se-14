package ru.chistyakov.tm.wrapper;


import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.UserDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "userDTOS")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Users {

    @XmlElement(name = "user")
    private List<UserDTO> userDTOS = null;

    @Nullable
    public List<UserDTO> getUserDTOS() {
        return userDTOS;
    }

    public void setUserDTOS(List<UserDTO> userDTOS) {
        this.userDTOS = userDTOS;
    }
}
