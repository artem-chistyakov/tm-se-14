package ru.chistyakov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.dto.ProjectDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "projectDTOS")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Projects {

    @XmlElement(name = "project")
    private List<ProjectDTO> projectDTOS = null;

    @Nullable
    public List<ProjectDTO> getProjectDTOS() {
        return projectDTOS;
    }

    public void setProjectDTOS(List<ProjectDTO> projectDTOS) {
        this.projectDTOS = projectDTOS;
    }
}
