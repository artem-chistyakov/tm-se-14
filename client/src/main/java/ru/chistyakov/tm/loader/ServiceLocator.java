package ru.chistyakov.tm.loader;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.*;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {
    @NotNull IProjectEndpoint getProjectEndpoint();

    @NotNull IUserEndpoint getUserEndpoint();

    @NotNull ITaskEndpoint getTaskEndpoint();

    @NotNull ISessionEndpoint getSessionEndpoint();

    @Nullable SessionDTO getSession();

    void setSession(@Nullable SessionDTO session);

    @NotNull Map<String, AbstractCommand> getCommandMap();

    @NotNull Scanner getScanner();
}
