package ru.chistyakov.tm.utility;

import lombok.experimental.UtilityClass;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

@UtilityClass
public final class DateParser {

    public static XMLGregorianCalendar getXMLGregorianCalendar(String date) {
        try {
            XMLGregorianCalendar xmlCalender = null;
            GregorianCalendar calender = new GregorianCalendar();
            calender.setTime(stringToJavaDate(date));
            xmlCalender = DatatypeFactory.newInstance().newXMLGregorianCalendar(calender);
            return xmlCalender;
        }catch (ParseException| DatatypeConfigurationException e){
            return null;
        }
    }
    private static Date stringToJavaDate(String sDate) throws ParseException {
        Date date = null;
        date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(sDate);
        return date;
    }
}
