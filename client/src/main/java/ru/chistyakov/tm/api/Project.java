
package ru.chistyakov.tm.api;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for project complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="project"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.tm.chistyakov.ru/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateBeginProject" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="dateEndProject" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="readinessStatus" type="{http://api.tm.chistyakov.ru/}readinessStatus" minOccurs="0"/&gt;
 *         &lt;element name="tasks" type="{http://api.tm.chistyakov.ru/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="user" type="{http://api.tm.chistyakov.ru/}user" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "project", propOrder = {
    "dateBeginProject",
    "dateEndProject",
    "description",
    "name",
    "readinessStatus",
    "tasks",
    "user"
})
public class Project
    extends AbstractEntity
{

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateBeginProject;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateEndProject;
    protected String description;
    protected String name;
    @XmlSchemaType(name = "string")
    protected ReadinessStatus readinessStatus;
    @XmlElement(nillable = true)
    protected List<Task> tasks;
    protected User user;

    /**
     * Gets the value of the dateBeginProject property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateBeginProject() {
        return dateBeginProject;
    }

    /**
     * Sets the value of the dateBeginProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateBeginProject(XMLGregorianCalendar value) {
        this.dateBeginProject = value;
    }

    /**
     * Gets the value of the dateEndProject property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateEndProject() {
        return dateEndProject;
    }

    /**
     * Sets the value of the dateEndProject property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateEndProject(XMLGregorianCalendar value) {
        this.dateEndProject = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the readinessStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ReadinessStatus }
     *     
     */
    public ReadinessStatus getReadinessStatus() {
        return readinessStatus;
    }

    /**
     * Sets the value of the readinessStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadinessStatus }
     *     
     */
    public void setReadinessStatus(ReadinessStatus value) {
        this.readinessStatus = value;
    }

    /**
     * Gets the value of the tasks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tasks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTasks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Task }
     * 
     * 
     */
    public List<Task> getTasks() {
        if (tasks == null) {
            tasks = new ArrayList<Task>();
        }
        return this.tasks;
    }

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setUser(User value) {
        this.user = value;
    }

}
