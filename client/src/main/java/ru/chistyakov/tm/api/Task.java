
package ru.chistyakov.tm.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for task complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="task"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.tm.chistyakov.ru/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateBeginTask" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="dateEndTask" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="project" type="{http://api.tm.chistyakov.ru/}project" minOccurs="0"/&gt;
 *         &lt;element name="readinessStatus" type="{http://api.tm.chistyakov.ru/}readinessStatus" minOccurs="0"/&gt;
 *         &lt;element name="user" type="{http://api.tm.chistyakov.ru/}user" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "task", propOrder = {
    "dateBeginTask",
    "dateEndTask",
    "description",
    "name",
    "project",
    "readinessStatus",
    "user"
})
public class Task
    extends AbstractEntity
{

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateBeginTask;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateEndTask;
    protected String description;
    protected String name;
    protected Project project;
    @XmlSchemaType(name = "string")
    protected ReadinessStatus readinessStatus;
    protected User user;

    /**
     * Gets the value of the dateBeginTask property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateBeginTask() {
        return dateBeginTask;
    }

    /**
     * Sets the value of the dateBeginTask property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateBeginTask(XMLGregorianCalendar value) {
        this.dateBeginTask = value;
    }

    /**
     * Gets the value of the dateEndTask property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateEndTask() {
        return dateEndTask;
    }

    /**
     * Sets the value of the dateEndTask property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateEndTask(XMLGregorianCalendar value) {
        this.dateEndTask = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the project property.
     * 
     * @return
     *     possible object is
     *     {@link Project }
     *     
     */
    public Project getProject() {
        return project;
    }

    /**
     * Sets the value of the project property.
     * 
     * @param value
     *     allowed object is
     *     {@link Project }
     *     
     */
    public void setProject(Project value) {
        this.project = value;
    }

    /**
     * Gets the value of the readinessStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ReadinessStatus }
     *     
     */
    public ReadinessStatus getReadinessStatus() {
        return readinessStatus;
    }

    /**
     * Sets the value of the readinessStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadinessStatus }
     *     
     */
    public void setReadinessStatus(ReadinessStatus value) {
        this.readinessStatus = value;
    }

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link User }
     *     
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link User }
     *     
     */
    public void setUser(User value) {
        this.user = value;
    }

}
