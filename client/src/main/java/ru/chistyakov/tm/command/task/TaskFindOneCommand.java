package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;


public final class TaskFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tfoc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск одной команды авторизованного пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id задачи");
        final TaskDTO task = serviceLocator.getTaskEndpoint().findOneTask(serviceLocator.getSession(), serviceLocator.getScanner().nextLine());
        if (task == null) throw new IllegalArgumentException("Ошибка поиска задачи");
        System.out.println("Task{" +
                " userId='" + task.getUserId() + '\'' +
                "projectId='" + task.getProjectId() + '\'' +
                ", id='" + task.getId() + '\'' +
                ", name='" + task.getName() + '\'' +
                ", readinessStatus=" + task.getReadinessStatus() +
                ", description='" + task.getDescription() + '\'' +
                ", dateBeginTask=" + task.getDateBeginTask() +
                ", dateEndTask=" + task.getDateEndTask() +
                '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
