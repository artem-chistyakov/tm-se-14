package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;


public final class ProjectFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pfoc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск одного проекта по id у авторизованного пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        final ProjectDTO project = serviceLocator.getProjectEndpoint().findOneProject(serviceLocator.getSession(), projectId);
        if (project == null) throw new IllegalArgumentException("Проект не найден");
        else
            System.out.println(serviceLocator.getProjectEndpoint().findOneProject(serviceLocator.getSession(), projectId));
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
