package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.ReadinessStatus;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.utility.DateParser;

import java.util.UUID;

public final class TaskInsertCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tic";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создает новую задачу";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите  id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите  название задачи");
        final String taskName = serviceLocator.getScanner().nextLine();
        System.out.println("Введите  описание задачи");
        final String descriptionTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите  дату начала задачи DD.MM.yyyy");
        final String dateBeginTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите  дату окончания задачи DD.MM.yyyy");
        final String dateEndTask = serviceLocator.getScanner().nextLine();
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(UUID.randomUUID().toString());
        taskDTO.setName(taskName);
        taskDTO.setDateBeginTask(DateParser.getXMLGregorianCalendar(dateBeginTask));
        taskDTO.setDateEndTask(DateParser.getXMLGregorianCalendar(dateEndTask));
        taskDTO.setDescription(descriptionTask);
        taskDTO.setProjectId(projectId);
        taskDTO.setUserId(serviceLocator.getSession().getUserId());
        taskDTO.setReadinessStatus(ReadinessStatus.PLANNED);
        if (serviceLocator.getTaskEndpoint().persistTask(serviceLocator.getSession(), taskDTO))
            System.out.println("Новая задача создана");
        else throw new IllegalArgumentException("Ошибка создания задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
