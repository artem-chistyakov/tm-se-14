package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public class UserDeleteAccountCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "udac";
    }

    @Override
    public @NotNull String getDescription() {
        return "Удаление аккаунта пользователя";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException {
        try {
            final String userId = serviceLocator.getSession().getUserId();
            serviceLocator.getUserEndpoint().deleteAccount(serviceLocator.getSession(), userId);
            serviceLocator.setSession(null);
        } catch (Exception e) {
            System.out.println("Ошибка удаления аккаунта");
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
