package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.ProjectDTO;
import ru.chistyakov.tm.api.ReadinessStatus;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.utility.DateParser;

public final class ProjectMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pmc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Если проекта не существует создает новый, иначе его обновляет";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите название проекта");
        final String nameProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите описание проекта");
        final String descriptionProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату начала проекта dd.MM.yyyy");
        final String dateBeginProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату окончания проекта dd.MM.yyyy");
        final String dateEndProject = serviceLocator.getScanner().nextLine();
        final ProjectDTO projectDTO = new ProjectDTO();
        System.out.println(projectDTO.getId());
        projectDTO.setId(projectId);
        projectDTO.setName(nameProject);
        projectDTO.setDescription(descriptionProject);
        projectDTO.setDateBeginProject(DateParser.getXMLGregorianCalendar(dateBeginProject));
        projectDTO.setDateEndProject(DateParser.getXMLGregorianCalendar(dateEndProject));
        projectDTO.setUserId(serviceLocator.getSession().getUserId());
        projectDTO.setReadinessStatus(ReadinessStatus.PLANNED);
        if (serviceLocator.getProjectEndpoint().persistProject(serviceLocator.getSession(), projectDTO))
            System.out.println("Новый проект успешно создан");
        else throw new IllegalArgumentException("Ошибка создания проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
