package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;

import java.util.Collection;

public final class TaskFindByPart extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "tfbp";
    }

    @Override
    public @NotNull String getDescription() {
        return "ищет задачи по части названия или описания";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println("Введите часть названия или описания");
        String part = serviceLocator.getScanner().nextLine();
        final Collection<TaskDTO> taskCollection = serviceLocator.getTaskEndpoint().findTaskByPartNameOrDescription(serviceLocator.getSession(), part);
        if (taskCollection.isEmpty()) throw new IllegalArgumentException("Задачи не найдены");
        for (final TaskDTO task : taskCollection)
            System.out.println("Task{" +
                    " userId='" + task.getUserId() + '\'' +
                    "projectId='" + task.getProjectId() + '\'' +
                    ", id='" + task.getId() + '\'' +
                    ", name='" + task.getName() + '\'' +
                    ", readinessStatus=" + task.getReadinessStatus() +
                    ", description='" + task.getDescription() + '\'' +
                    ", dateBeginTask=" + task.getDateBeginTask() +
                    ", dateEndTask=" + task.getDateEndTask() +
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
