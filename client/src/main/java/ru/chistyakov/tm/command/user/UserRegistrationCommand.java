package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.UserDTO;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.utility.PasswordParser;

import java.util.UUID;

public final class UserRegistrationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "urc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "регистрация пользователя";
    }

    @Override
    public void execute() {
        System.out.println("введите имя");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.println("введите пароль");
        final String password = serviceLocator.getScanner().nextLine();
        final UserDTO userDTO = new UserDTO();
        userDTO.setId(UUID.randomUUID().toString());
        userDTO.setLogin(login);
        userDTO.setPassword(PasswordParser.parse(password));
        userDTO.setRoleType(RoleType.USUAL_USER);
        if (serviceLocator.getUserEndpoint().registryUser(userDTO))
            System.out.println("Пользователь зарегистрирован");
        else System.out.println("Ошибка регистрации пользователя");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{null};
    }
}
