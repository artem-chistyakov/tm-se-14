package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public final class UserEndSessionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "uesc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Заканчивает пользовательскую сессию";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        serviceLocator.setSession(null);
        System.out.println("Пользовательская сессия окончена");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
