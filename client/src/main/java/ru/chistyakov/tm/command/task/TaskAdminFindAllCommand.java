package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.api.TaskDTO;
import ru.chistyakov.tm.command.AbstractCommand;

public final class TaskAdminFindAllCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "tafac";
    }

    @Override
    public @NotNull String getDescription() {
        return "Выводит все задачи всех пользователей";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        for (final TaskDTO task : serviceLocator.getTaskEndpoint().findAllTask(serviceLocator.getSession()))
            System.out.println("Task{" +
                    " userId='" + task.getUserId() + '\'' +
                    "projectId='" + task.getProjectId() + '\'' +
                    ", id='" + task.getId() + '\'' +
                    ", name='" + task.getName() + '\'' +
                    ", readinessStatus=" + task.getReadinessStatus() +
                    ", description='" + task.getDescription() + '\'' +
                    ", dateBeginTask=" + task.getDateBeginTask() +
                    ", dateEndTask=" + task.getDateEndTask() +
                    '}');
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
