package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;

public class UserDeserializateDomen extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "udd";
    }

    @Override
    public @NotNull String getDescription() {
        return "Загрузка предметной области с использованием десериализации";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        serviceLocator.getUserEndpoint().deserializateDomain(serviceLocator.getSession());
    }


    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
