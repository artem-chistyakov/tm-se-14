package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.RoleType;
import ru.chistyakov.tm.command.AbstractCommand;


public class UserLookProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "ulpc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Просмотр профиля пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getSession() == null)
            throw new NullPointerException("Текущий пользователь не авторизован");
        System.out.println(serviceLocator.getSession().getUserId());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};

    }
}
